package br.com.iteris.data.jpa.spec.auto.metadata;

import br.com.iteris.data.jpa.spec.auto.internal.Operation;
import br.com.iteris.data.jpa.spec.auto.internal.SpecField;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SpecProperty {
    String value() default "";

    Operation operation() default Operation.UNDEFINED;

    SpecFunction[] functions() default {};
}
