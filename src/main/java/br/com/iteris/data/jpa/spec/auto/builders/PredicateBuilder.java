package br.com.iteris.data.jpa.spec.auto.builders;

import br.com.iteris.data.jpa.spec.auto.utils.PathUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PredicateBuilder {

    private List<Predicate> predicates = new ArrayList<>();
    private CriteriaBuilder builder;
    private Root root;
    private CriteriaQuery query;

    public PredicateBuilder(CriteriaBuilder builder, Root root, CriteriaQuery query) {
        this.builder = builder;
        this.root = root;
        this.query = query;
    }

    public PredicateBuilder eq(String attr, Object value) {
        if (value != null)
        add(builder.equal(PathUtils.getPath(root, builder, attr), value));
        return this;
    }

    public PredicateBuilder in(String attr, List<?> values) {
        if (values != null && !values.isEmpty()) {
            if (values.size() > 1000) {
                final Predicate disjunction = builder.disjunction();
                ListUtils.partition(values, 1000).stream()
                        .map(page -> PathUtils.getPath(root, builder, attr).in(page))
                        .forEach(disjunction.getExpressions()::add);
                add(disjunction);
            } else {
                add (PathUtils.getPath(root, builder, attr).in(values));
            }
        }

        return this;
    }

    public PredicateBuilder like(String attr, String value) {
        if (StringUtils.isNotBlank(value)) {
            add(builder.like(PathUtils.getPath(root, builder, attr), "%" + value + "%"));
        }
        return this;
    }

    public PredicateBuilder startsWith(String attr, String value) {
        if (StringUtils.isNotBlank(value)) {
            add(builder.like(PathUtils.getPath(root, builder, attr), value + "%"));
        }
        return this;
    }

    public PredicateBuilder endsWith(String attr, String value) {
        if (StringUtils.isNotBlank(value)) {
            add(builder.like(PathUtils.getPath(root, builder, attr), "%" + value));
        }
        return this;
    }

    public PredicateBuilder range(String attr, Comparable start, Comparable end) {
        return range(attr, start, true, end, true);
    }

    public PredicateBuilder range(String attr, Comparable start, boolean startInclusive, Comparable end, boolean endInclusive) {
        return greaterThan(attr, start, startInclusive)
            .lesserThan(attr, end, endInclusive);
    }

    public PredicateBuilder greaterThan(String attr, Comparable comp, Boolean inclusive) {
        Expression path = PathUtils.getPath(root, builder, attr);
        Predicate expression = null;
        if (comp != null) {
            if (inclusive) {
                expression = builder.greaterThanOrEqualTo(path, comp);
            } else {
                expression = builder.greaterThan(path, comp);
            }
        }

        Optional.ofNullable(expression).ifPresent(this::add);
        return this;
    }

    public PredicateBuilder lesserThan(String attr, Comparable comp, Boolean inclusive) {
        Expression path = PathUtils.getPath(root, builder, attr);
        Predicate expression = null;
        if (comp != null) {
            if (inclusive) {
                expression = builder.lessThanOrEqualTo(path, comp);
            } else {
                expression = builder.lessThan(path, comp);
            }
        }

        Optional.ofNullable(expression).ifPresent(this::add);
        return this;
    }

    public PredicateBuilder add(Predicate newPredicate) {
        predicates.add(newPredicate);
        return this;
    }

    public Predicate build() {
        return builder.and(predicates.toArray(new Predicate[0]));
    }

    public Predicate buildOr() {
        return builder.or(predicates.toArray(new Predicate[0]));
    }
}
