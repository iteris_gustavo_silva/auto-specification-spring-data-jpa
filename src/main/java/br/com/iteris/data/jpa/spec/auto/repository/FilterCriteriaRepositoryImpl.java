package br.com.iteris.data.jpa.spec.auto.repository;

import static org.springframework.data.jpa.repository.query.QueryUtils.toOrders;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.LongSupplier;
import java.util.function.Predicate;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.query.Jpa21Utils;
import org.springframework.data.jpa.repository.query.JpaEntityGraph;
import org.springframework.data.jpa.repository.support.CrudMethodMetadata;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.data.util.Optionals;

import br.com.iteris.data.jpa.spec.auto.filter.TransferFilter;

public class FilterCriteriaRepositoryImpl<T, I extends Serializable> extends SimpleJpaRepository<T, I>
        implements FilterCriteriaRepository<T, I> {
	
    private EntityManager entityManager;
    private Class<T> domainClass;
    private static final Predicate<List<?>> EMPTY_LIST = Collection::isEmpty;
    private final JpaEntityInformation<T, ?> entityInformation;

    protected FilterCriteriaRepositoryImpl(JpaEntityInformation<T, ?> entityInformation,
            EntityManager entityManager) {
    	super(entityInformation, entityManager);
    	this.entityManager = entityManager;
		this.entityInformation = entityInformation;
		domainClass = entityInformation.getJavaType();
    }

    @Override
    public <D> D findByFilter(TransferFilter<T, D> filter) {
        TypedQuery<D> typedQuery = buildTypedQuery(filter, null);
        List<D> resultList = typedQuery.getResultList();
        return Optional.ofNullable(resultList)
                .filter(EMPTY_LIST.negate())
                .map(l -> l.get(0))
                .orElse(null);
    }

    @Override
    public <D> List<D> findAll(TransferFilter<T, D> filter) {
        TypedQuery<D> typedQuery = buildTypedQuery(filter, null);
        return typedQuery.getResultList();
    }

    @Override
    public <D> Page<D> findAll(TransferFilter<T, D> filter, Pageable pageable) {
        return findAll(filter, pageable, executeCount(filter));
    }

    public <D> Page<D> findAll(TransferFilter<T, D> filter, Pageable pageable, LongSupplier totalSupplier) {
        if (null == pageable) {
            return new PageImpl<>(findAll(filter));
        }

        TypedQuery<D> query = buildTypedQuery(filter, pageable.getSort());
        query.setFirstResult((int)pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());

        return PageableExecutionUtils.getPage(query.getResultList(), pageable, totalSupplier);
    }

    private LongSupplier executeCount(TransferFilter<T, ?> filter) {
        return () -> {
            TypedQuery<Long> query = buildCountQuery(filter);
            List<Long> totals = query.getResultList();
            Long total = 0L;

            for (Long element : totals) {
                total += element == null ? 0 : element;
            }

            return total;
        };
    }

    private <D> TypedQuery<D> buildTypedQuery(TransferFilter<T, D> filter, Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @SuppressWarnings("unchecked")
		CriteriaQuery<D> query = (CriteriaQuery<D>) criteriaBuilder.createQuery();
        Root<T> root = query.from(domainClass);
        query.where(filter.toSpecification().toPredicate(root, query, criteriaBuilder));

        List<Order> orders = new ArrayList<>();
        if (sort != null) {
            orders.addAll(toOrders(sort, root, criteriaBuilder));
        }

        orders.addAll(filter.defineSort(root, criteriaBuilder));
        query.orderBy(orders);

        filter.defineSelection(root, query, criteriaBuilder);

        return applyRepositoryMethodMetadata(entityManager.createQuery(query));
    }

    private TypedQuery<Long> buildCountQuery(TransferFilter<T, ?> filter) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
        Root<T> root = query.from(domainClass);
        query.where(filter.toSpecification().toPredicate(root, query, criteriaBuilder));

        query.select(criteriaBuilder.count(root));
        return entityManager.createQuery(query);
    }

    private <S> TypedQuery<S> applyRepositoryMethodMetadata(TypedQuery<S> query) {

        CrudMethodMetadata metadata = getRepositoryMethodMetadata();

        if (getRepositoryMethodMetadata() == null) {
            return query;
        }

        LockModeType type = metadata.getLockModeType();
        TypedQuery<S> toReturn = type == null ? query : query.setLockMode(type);

        applyQueryHints(toReturn);

        return toReturn;
    }

    private void applyQueryHints(Query query) {  
        for (Map.Entry<String, Object> hint : getCustomQueryHints().entrySet()) {
            query.setHint(hint.getKey(), hint.getValue());
        }
    }
    
	public Map<String, Object> getCustomQueryHints() {

		Map<String, Object> hints = new HashMap<>();

		hints.putAll(getRepositoryMethodMetadata().getQueryHints());
		hints.putAll(getFetchGraphs());

		return hints;
	}

	private Map<String, Object> getFetchGraphs() {
		return Optionals
				.mapIfAllPresent(Optional.of(entityManager), getRepositoryMethodMetadata().getEntityGraph(),
						(em, graph) -> Jpa21Utils.tryGetFetchGraphHints(em, getEntityGraph(graph), entityInformation.getJavaType()))
				.orElse(Collections.emptyMap());
	}
	
	private JpaEntityGraph getEntityGraph(EntityGraph graph) {

		String fallbackName = entityInformation.getEntityName() + "." + getRepositoryMethodMetadata().getMethod().getName();
		return new JpaEntityGraph(graph, fallbackName);
	}
}
