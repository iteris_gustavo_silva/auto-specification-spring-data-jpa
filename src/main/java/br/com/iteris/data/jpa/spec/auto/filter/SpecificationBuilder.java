package br.com.iteris.data.jpa.spec.auto.filter;

import br.com.iteris.data.jpa.spec.auto.internal.FieldValue;
import br.com.iteris.data.jpa.spec.auto.internal.FunctionCall;
import br.com.iteris.data.jpa.spec.auto.internal.Operation;
import br.com.iteris.data.jpa.spec.auto.internal.SpecField;
import br.com.iteris.data.jpa.spec.auto.metadata.SpecAlternative;
import br.com.iteris.data.jpa.spec.auto.metadata.SpecFunctionParam;
import br.com.iteris.data.jpa.spec.auto.metadata.SpecProperty;
import br.com.iteris.data.jpa.spec.auto.metadata.SpecRange;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public class SpecificationBuilder<T, U, X extends Filter<T>> {

    private static final List<Class> HANDLED_ANNOTATIONS = Arrays.asList(SpecProperty.class, SpecAlternative.class, SpecRange.class);

    private final CriteriaBuilder builder;
    private final Root<T> root;
    private final CriteriaQuery<U> query;

    public Predicate toPredicate(X specificationSource) {
        Set<SpecField> fieldMetadata = buildMeta(specificationSource);
        Predicate conjunction = builder.conjunction();

        fieldMetadata.stream()
            .map(this::processSpecField)
            .forEach(conjunction.getExpressions()::add);

        return conjunction;
    }

    private Predicate processSpecField(SpecField<?> field) {
        Predicate main = field.getOperation().apply(builder, root, field);
        if (!field.getAlternatives().isEmpty()) {
            Predicate predicate = builder.disjunction();
            predicate.getExpressions().add(main);

            field.getAlternatives().forEach(consumeAlternative(predicate, field));

            return predicate;
        }

        return main;
    }

    private Consumer<? super String> consumeAlternative(Predicate predicate, SpecField field) {
        return (alt) -> {
            SpecField altField = field.copyAlt(alt);
            predicate.getExpressions().add(field.getOperation().apply(builder, root, altField));
        };
    }

    private Set<SpecField> buildMeta(X specificationSource) {
        List<SpecField> specFields = Stream.of(specificationSource.getClass().getDeclaredFields())
                .filter(this::containsAnnotations)
                .map(f -> new FieldValue(f, getValue(f, specificationSource)))
                .map(this::buildSpec)
                .filter(SpecField::notEmpty)
                .collect(Collectors.toList());

        return specFields.stream().reduce(new HashSet<>(), this::accumulate, this::combine);
    }

    private HashSet<SpecField> combine(HashSet<SpecField> s1, HashSet<SpecField> s2) {
        return s1;
    }

    private HashSet<SpecField> accumulate(HashSet<SpecField> fields, SpecField specField) {
        java.util.function.Predicate<SpecField> matchPredicate = sp -> sp.equalsIgnoreOperation(specField);
        SpecField rangedField = fields.stream().filter(matchPredicate).findFirst().orElse(null);
        if (rangedField != null && rangedField.canCombineWith(specField)) {
            rangedField.combineWith(specField);
        } else {
            fields.add(specField);
        }

        return fields;
    }


    private Object getValue(Field field, X specificationSource) {
        try {
            Boolean isAccessible = field.isAccessible();
            field.setAccessible(true);
            Object value = field.get(specificationSource);
            field.setAccessible(isAccessible);
            return value;
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Error getting value fia collection");
        }
    }

    private SpecField buildSpec(FieldValue fieldValue) {
        Field field = fieldValue.getField();

        Operation operation = Stream.of(Operation.validValues())
                .filter(oper -> field.getName().endsWith(oper.getOpName()))
                .findFirst()
                .orElse(Operation.EQ);

        String processedFieldName = field.getName().replaceAll(operation.getOpName(), "");
        Map<String, FunctionCall> functions = new LinkedHashMap<>();
        if (field.isAnnotationPresent(SpecProperty.class)) {
            SpecProperty specAnnotation = field.getAnnotation(SpecProperty.class);
            processedFieldName = specAnnotation.value().isEmpty() ? processedFieldName : specAnnotation.value();

            operation = Operation.UNDEFINED.equals(specAnnotation.operation()) ? operation: specAnnotation.operation();

            Stream.of(specAnnotation.functions())
                .map(f -> FunctionCall.builder()
                        .name(f.name())
                        .params(Arrays.asList(f.params()))
                        .expectedType(f.expectedType())
                        .root(f.root())
                        .build())
                .forEach(fc ->
                    functions.put(fc.getName(),fc)
                );
        }

        List<String> alternatives = new ArrayList<>();
        if (field.isAnnotationPresent(SpecAlternative.class)) {
            SpecAlternative alternative = field.getAnnotation(SpecAlternative.class);
            alternatives.addAll(Arrays.asList(alternative.value()));
        }

        SpecField.SpecFieldBuilder<Object> builder = SpecField.builder()
                .fieldName(processedFieldName)
                .alternatives(alternatives)
                .functions(functions)
                .operation(operation);

        Boolean isRange = field.isAnnotationPresent(SpecRange.class);
        builder.range(isRange);
        if (isRange) {
            handleRange(fieldValue, builder);
        } else {
            if (fieldValue.valueIsCollection()) {
                builder.values((Collection) fieldValue.getValue());
            } else {
                builder.value(fieldValue.getValue());
            }
        }

        return builder.build();
    }

    private void handleRange(FieldValue fieldValue, SpecField.SpecFieldBuilder<Object> builder) {
        if (Objects.nonNull(fieldValue.getValue())) {
            Assert.isAssignable(Comparable.class, fieldValue.getValue().getClass());

            Field field = fieldValue.getField();
            SpecRange annotation = field.getAnnotation(SpecRange.class);
            builder.fieldName(annotation.value());

            if (field.getName().startsWith("start")) {
                builder.rangeStartValue(fieldValue.getValue());
                builder.operation(findRangeStartOperation(annotation));
            } else if (field.getName().startsWith("end")) {
                builder.rangeEndValue(fieldValue.getValue());
                builder.operation(findRangeEndAnnotation(annotation));
            }
        }
    }

    private Operation findRangeStartOperation(SpecRange annotation) {
        if (Operation.UNDEFINED.equals(annotation.operation())) {
            return Operation.GE;
        }

        return validateOperation(annotation.operation(), "start", Operation.GT, Operation.GE);
    }

    private Operation findRangeEndAnnotation(SpecRange annotation) {
        if (Operation.UNDEFINED.equals(annotation.operation())) {
            return Operation.LE;
        }

        return validateOperation(annotation.operation(), "end", Operation.LT, Operation.LE);
    }

    private Operation validateOperation(Operation operation, String rangeType, Operation... allowedOperations) {
        boolean found = Stream.of(allowedOperations).anyMatch(op -> op.equals(operation));

        if (!found) {
            String validOperations = Stream.of(allowedOperations).map(Enum::name).collect(Collectors.joining(", "));
            throw new IllegalStateException("This operation is not valid in " + rangeType + " of range. Use one of " + validOperations + " instead");
        }

        return operation;
    }

    private boolean containsAnnotations(Field field) {
        return HANDLED_ANNOTATIONS.stream().anyMatch(field::isAnnotationPresent);
    }
}
