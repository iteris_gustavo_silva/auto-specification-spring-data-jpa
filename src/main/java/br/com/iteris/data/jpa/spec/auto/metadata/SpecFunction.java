package br.com.iteris.data.jpa.spec.auto.metadata;

public @interface SpecFunction {

    String name();

    String[] params() default "{{field}}";

    Class expectedType();

    boolean root() default false;
}
