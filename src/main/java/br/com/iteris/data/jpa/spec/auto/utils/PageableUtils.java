package br.com.iteris.data.jpa.spec.auto.utils;

import org.springframework.data.domain.Pageable;

import java.util.function.LongSupplier;

public class PageableUtils {

    private PageableUtils() { }
    
    public static LongSupplier hasNextPageable(Integer currentPageSize, Pageable pageable) {
        return () -> {
            if (currentPageSize < pageable.getPageSize()) {
                return pageable.getOffset() + currentPageSize;
            }

            return pageable.getOffset() + currentPageSize + 1;
        };
    }

    public static LongSupplier emptyPageable() {
        return () -> 0L;
    }
}
