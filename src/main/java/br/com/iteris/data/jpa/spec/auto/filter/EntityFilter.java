package br.com.iteris.data.jpa.spec.auto.filter;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public interface EntityFilter<T> extends Filter<T> {

    default Specification<T> toSpecification() {
        return (Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) ->
            new SpecificationBuilder<>(builder, root, query).toPredicate(this);
    }
}
