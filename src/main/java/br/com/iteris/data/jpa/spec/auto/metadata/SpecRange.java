package br.com.iteris.data.jpa.spec.auto.metadata;

import br.com.iteris.data.jpa.spec.auto.internal.Operation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface SpecRange {
    String value();
    Operation operation() default Operation.UNDEFINED;
}
