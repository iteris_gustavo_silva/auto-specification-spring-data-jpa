package br.com.iteris.data.jpa.spec.auto.internal;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.lang.reflect.Field;
import java.util.Collection;

@AllArgsConstructor
public class FieldValue {

    @Getter
    private Field field;

    @Getter
    private Object value;

    public boolean valueIsCollection() {
        return value instanceof Collection;
    }
}
