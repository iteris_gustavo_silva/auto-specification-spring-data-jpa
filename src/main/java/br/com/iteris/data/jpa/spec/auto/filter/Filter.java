package br.com.iteris.data.jpa.spec.auto.filter;

import org.springframework.data.jpa.domain.Specification;

public interface Filter<T> {

    Specification<T> toSpecification();
}
