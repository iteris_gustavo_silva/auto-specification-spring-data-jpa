package br.com.iteris.data.jpa.spec.auto.internal;

import br.com.iteris.data.jpa.spec.auto.utils.PathUtils;
import org.apache.commons.collections4.ListUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

class Specifications {

    @FunctionalInterface
    interface SpecificationFunction {

        Predicate apply(CriteriaBuilder builder, Root<?> root, SpecField<?> spec);
    }
    static Predicate equalsSpec(CriteriaBuilder builder, Root<?> root, SpecField<?> spec) {
        return builder.equal(PathUtils.getPath(root, builder, spec.getFieldName(), spec.getFunctions()), spec.getValue());
    }

    static Predicate likeSpec(CriteriaBuilder builder, Root<?> root, SpecField<?> spec) {
        String pattern = MessageFormat.format("%{0}%", String.valueOf(spec.getValue()));
        return builder.like(PathUtils.getPath(root, builder, spec.getFieldName(), spec.getFunctions()), pattern);
    }

    static Predicate startsWithSpec(CriteriaBuilder builder, Root<?> root, SpecField<?> spec) {
        String pattern = MessageFormat.format("{0}%", String.valueOf(spec.getValue()));
        return builder.like(PathUtils.getPath(root, builder, spec.getFieldName(), spec.getFunctions()), pattern);
    }

    static Predicate endsWithSpec(CriteriaBuilder builder, Root<?> root, SpecField<?> spec) {
        String pattern = MessageFormat.format("%{0}", String.valueOf(spec.getValue()));
        return builder.like(PathUtils.getPath(root, builder, spec.getFieldName(), spec.getFunctions()), pattern);
    }

    static Predicate inSpec(CriteriaBuilder builder, Root<?> root, SpecField<?> spec) {
        List<?> values = (List<?>) spec.getValues();
        if (values.size() > 1000) {
            final Predicate disjunction = builder.disjunction();
            ListUtils.partition(values, 1000).stream()
                .map(page -> PathUtils.getPath(root, builder, spec.getFieldName(), spec.getFunctions()).in(page))
                .forEach(disjunction.getExpressions()::add);

            return disjunction;
        }

        return PathUtils.getPath(root, builder, spec.getFieldName(), spec.getFunctions()).in(values);
    }

    static Predicate betweenSpec(CriteriaBuilder builder, Root<?> root, SpecField<?> spec) {
        Comparable start = (Comparable) spec.getRangeStartValue();
        Comparable end = (Comparable) spec.getRangeEndValue();
        return builder.between(PathUtils.getPath(root, builder, spec.getFieldName(), spec.getFunctions()), start, end);
    }

    static Predicate greaterThanSpec(CriteriaBuilder builder, Root<?> root, SpecField<?> spec) {
        Comparable value = getComparable(spec, SpecField::getRangeStartValue);
        return builder.greaterThan(PathUtils.getPath(root, builder, spec.getFieldName(), spec.getFunctions()), value);
    }

    static Predicate greaterOrEqualThanSpec(CriteriaBuilder builder, Root<?> root, SpecField<?> spec) {
        Comparable value = getComparable(spec, SpecField::getRangeStartValue);
        return builder.greaterThanOrEqualTo(PathUtils.getPath(root, builder, spec.getFieldName(), spec.getFunctions()), value);
    }

    static Predicate lesserThanSpec(CriteriaBuilder builder, Root<?> root, SpecField<?> spec) {
        Comparable value = getComparable(spec, SpecField::getRangeEndValue);
        return builder.lessThan(PathUtils.getPath(root, builder, spec.getFieldName(), spec.getFunctions()), value);
    }

    static Predicate lesserOrEqualThanSpec(CriteriaBuilder builder, Root<?> root, SpecField<?> spec) {
        Comparable value = getComparable(spec, SpecField::getRangeEndValue);
        return builder.lessThanOrEqualTo(PathUtils.getPath(root, builder, spec.getFieldName(), spec.getFunctions()), value);
    }

    private static Comparable getComparable(SpecField<?> spec, Function<SpecField, Object> rangeFunction) {
        return (Comparable) Optional.ofNullable(rangeFunction.apply(spec)).orElse(spec.getValue());
    }

    static Predicate noOpSpec(CriteriaBuilder criteriaBuilder, Root<?> root, SpecField<?> specField) {
        throw new IllegalStateException("Should not call this method");
    }
}
