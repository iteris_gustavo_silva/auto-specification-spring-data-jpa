package br.com.iteris.data.jpa.spec.auto.internal;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class FunctionCall {

    private String name;
    private Class<?> expectedType;
    private List<String> params;
    private boolean root;
}
