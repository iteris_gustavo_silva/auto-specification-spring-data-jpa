package br.com.iteris.data.jpa.spec.auto.utils;

import br.com.iteris.data.jpa.spec.auto.internal.FunctionCall;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class PathUtils {

    public static <X> Expression<X> getPath(Root<X> root, CriteriaBuilder builder, String fieldName) {
        return getPath(root, builder, fieldName, null);
    }

    public static <V, X> Expression<V> getPath(Root<X> root, CriteriaBuilder builder, String fieldName, Map<String, FunctionCall> functions) {
        String[] split = fieldName.split("_");
        Path path = root;

        for (String str : split) {
            path = path.get(str);

            if (List.class.equals(path.type().getJavaType())
                    || Set.class.equals(path.type().getJavaType())) {
                path = getJoin(root, str);
            }
        }

        if (Objects.nonNull(functions) && !functions.isEmpty()) {
            Path safePath = path;
            Map<String, Expression> functionCalls = new LinkedHashMap<>();
            List<Expression> rootExpression = new ArrayList<>();

            functions.forEach((name, fc) -> {
                Expression expression = builder.function(name, fc.getExpectedType(), getParams(fc.getParams(), functionCalls, safePath, builder));
                functionCalls.put(name, expression);

                if (fc.isRoot()) {
                    rootExpression.add(expression);
                }
            });

            Optional<Expression> fromMap = functionCalls.values().stream().findFirst();
            return rootExpression.stream().findFirst().orElse(fromMap.orElse(path));
        }

        return path;
    }

    private static <V, X> Join<V, ?> getJoin(Root<X> root, String name) {
        if (root.getJoins().isEmpty()) {
            return root.join(name);
        }

        Join<X, ?> join = root.getJoins().stream()
                                .filter(j -> j.getAttribute().getName().equals(name))
                                .findFirst()
                                .orElseGet(() -> root.join(name));

        return (Join<V, ?>) join;
    }

    private static Expression[] getParams(List<String> params, Map<String, Expression> functionMap, Path field, CriteriaBuilder builder) {
        return params.stream()
            .map(p -> {
                if (p.matches("\\{\\{field}}")) {
                    return field;
                } else if (p.matches("\\{\\{fc:([A-Za-z0-9-_]+)}}")) {
                    String innerFunctionName = p.replaceAll("\\{\\{fc:([A-Za-z0-9-_]+)}}", "$1");
                    return functionMap.get(innerFunctionName);
                }

                return builder.literal(p);
            })
            .toArray(Expression[]::new);
    }
}
