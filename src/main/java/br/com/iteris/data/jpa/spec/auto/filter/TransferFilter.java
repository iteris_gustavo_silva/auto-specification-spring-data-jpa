package br.com.iteris.data.jpa.spec.auto.filter;

import java.util.Collections;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public interface TransferFilter<T, X> extends Filter<T> {
    default Specification<T> toSpecification() {
        return (Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) ->
            new SpecificationBuilder<>(builder, root, query).toPredicate(this);
    }

    CriteriaQuery<X> defineSelection(Root<T> root, CriteriaQuery<X> query, CriteriaBuilder builder);

    default List<Order> defineSort(Root<T> root, CriteriaBuilder builder) {
        return Collections.emptyList();
    }
}
