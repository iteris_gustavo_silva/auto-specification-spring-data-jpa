package br.com.iteris.data.jpa.spec.auto.metadata;

public @interface SpecFunctionParam {

    String value() default "{{field}}";

    Class type() default Object.class;
}
