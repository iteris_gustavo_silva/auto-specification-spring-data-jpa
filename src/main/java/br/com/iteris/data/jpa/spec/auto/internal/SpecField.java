package br.com.iteris.data.jpa.spec.auto.internal;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.EqualsBuilder;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

@Builder
@Data
@EqualsAndHashCode(exclude = {"value", "values", "rangeStartValue", "rangeEndValue"})
public class SpecField<T> {

    private String fieldName;
    private List<String> alternatives;
    private T value;
    private T rangeStartValue;
    private T rangeEndValue;
    private Collection<T> values;
    private Operation operation;
    private Map<String, FunctionCall> functions;
    private Boolean range;

    public boolean notEmpty() {
        if (range) {
            return Objects.nonNull(rangeStartValue) || Objects.nonNull(rangeEndValue);
        }

        return Objects.nonNull(value) || (Objects.nonNull(values) && !values.isEmpty());
    }

    public SpecField combineWith(SpecField<Comparable> specField) {
        if (this.range && specField.range) {
            boolean startInclusive = false;
            boolean endInclusive = false;

            if (Objects.nonNull(this.rangeStartValue) && Objects.nonNull(specField.rangeEndValue)) {
                this.rangeEndValue = (T) specField.rangeEndValue;
                startInclusive = Operation.GE.equals(operation);
                endInclusive = Operation.LE.equals(specField.operation);
            } else if (Objects.nonNull(this.rangeEndValue) && Objects.nonNull(specField.rangeStartValue)) {
                this.rangeStartValue = (T) specField.rangeStartValue;
                startInclusive = Operation.GE.equals(specField.operation);
                endInclusive = Operation.LE.equals(operation);
            }

            redefineOperation(startInclusive, endInclusive);
        }
        return this;
    }

    private void redefineOperation(boolean startInclusive, boolean endInclusive) {
        if (startInclusive && endInclusive) {
            this.operation = Operation.BETWEEN;
        }
    }

    public SpecField<T> copyAlt(String alternative) {
        return SpecField.<T>builder()
                .fieldName(alternative)
                .operation(operation)
                .value(value)
                .rangeStartValue(rangeStartValue)
                .rangeEndValue(rangeEndValue)
                .values(values)
                .range(range)
                .functions(functions)
                .build();
    }

    public boolean equalsIgnoreOperation(SpecField specField) {
        return new EqualsBuilder()
            .append(fieldName, specField.fieldName)
            .append(alternatives, specField.alternatives)
            .append(range, specField.range)
            .build();
    }

    public boolean canCombineWith(SpecField specField) {
        Predicate<Operation> match = operation::equals;
        match = match.or(specField.operation::equals);
        return Operation.COMBINING_OPERATIONS.stream().allMatch(match);
    }
}
