package br.com.iteris.data.jpa.spec.auto.repository;

import java.io.Serializable;
import java.util.List;
import java.util.function.LongSupplier;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import br.com.iteris.data.jpa.spec.auto.filter.TransferFilter;

@NoRepositoryBean
public interface FilterCriteriaRepository<E, ID extends Serializable> extends JpaRepository<E, ID>, JpaSpecificationExecutor<E>  {

    <D> D findByFilter(TransferFilter<E, D> filter);

    <D> List<D> findAll(TransferFilter<E, D> filter);

    <D> Page<D> findAll(TransferFilter<E, D> filter, Pageable pageable);

    <D> Page<D> findAll(TransferFilter<E, D> filter, Pageable pageable, LongSupplier totalSupplier);
}
