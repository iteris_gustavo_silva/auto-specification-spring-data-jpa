package br.com.iteris.data.jpa.spec.auto.internal;

import lombok.Getter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public enum Operation {
    UNDEFINED(null, Specifications::noOpSpec),
    EQ("Equal", Specifications::equalsSpec),
    LIKE("Like", Specifications::likeSpec),
    IN("In", Specifications::inSpec),
    BETWEEN("Between", Specifications::betweenSpec),
    GT("GreaterThan", Specifications::greaterThanSpec),
    GE("GreaterOrEqualThan", Specifications::greaterOrEqualThanSpec),
    LT("LesserThan", Specifications::lesserThanSpec),
    LE("LesserOrEqualThan", Specifications::lesserOrEqualThanSpec),
    STARTS_WITH("StartsWith", Specifications::startsWithSpec),
    ENDS_WITH("EndsWith", Specifications::endsWithSpec),
    ;

    public static final List<Operation> COMBINING_OPERATIONS = Arrays.asList(LE, GE);
    private static final java.util.function.Predicate<Operation> IS_UNDEFINED = UNDEFINED::equals;
    private static final Operation[] VALID = Stream.of(values())
            .filter(IS_UNDEFINED.negate())
            .toArray(Operation[]::new);

    @Getter
    private final String opName;
    private final Specifications.SpecificationFunction applyFunction;

    Operation(String opName, Specifications.SpecificationFunction applyFunction) {
        this.opName = opName;
        this.applyFunction = applyFunction;
    }

    public static Operation[] validValues() {
        return VALID;
    }

    public Predicate apply(CriteriaBuilder builder, Root<?> root, SpecField<?> spec) {
        return applyFunction.apply(builder, root, spec);
    }

}
